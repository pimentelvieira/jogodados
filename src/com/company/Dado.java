package com.company;

public class Dado {

    private int lados;

    public Dado(int lados) {
        this.lados = lados;
    }

    public int getLados() {
        return lados;
    }
}
