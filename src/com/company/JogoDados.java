package com.company;

import java.util.Random;

public class JogoDados {

    private int[] numerosSorteados;

    public void sortear(int lados, int qtdSorteios) {
        Random random = new Random();
        numerosSorteados = new int[qtdSorteios];
        for (int i = 0; i < qtdSorteios; i++) {
            this.numerosSorteados[i] = random.nextInt(lados) + 1;
        }
    }

    public int calcularSoma() {
        int soma = 0;

        for (int i = 0; i < this.numerosSorteados.length; i++) {
            soma += this.numerosSorteados[i];
        }

        return soma;
    }

    public void imprimir() {
        String msg = "";

        for (int i = 0; i < this.numerosSorteados.length; i++) {
            msg += this.numerosSorteados[i] + ", ";
        }

        System.out.println(msg + calcularSoma());
    }
}
